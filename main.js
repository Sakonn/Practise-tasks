'use strict';

const electron = require('electron');
const { app, BrowserWindow } = require('electron');
require('electron-reload')(__dirname);

let win;

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({ width: 800, height: 400 });

  // and load the index.html of the app.
  win.loadFile('index.html');

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
      app.quit()
  }
});
